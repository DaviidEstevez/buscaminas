package buscamines;

import buscamines.BuscaminesContract.BuscaminesModel;
import buscamines.BuscaminesContract.BuscaminesPresenter;
import buscamines.BuscaminesContract.BuscaminesView;
import buscamines.impl.BuscaminesModelImpl;
import buscamines.impl.BuscaminesPresenterImpl;
import buscamines.impl.BuscaminesViewImpl;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author usuari
 */
public class BuscaminesApp extends Application {

    @Override
    public void start(Stage stage) throws IOException {

        BuscaminesView v;
        BuscaminesPresenter p;
        BuscaminesModel m;

        p = new BuscaminesPresenterImpl();
        m = new BuscaminesModelImpl();
        v = new BuscaminesViewImpl(stage);

        p.setView(v);
        p.setModel(m);
        v.setPresenter(p);
        
    }

    public static void main(String[] args) {
        launch(args);
    }

}
