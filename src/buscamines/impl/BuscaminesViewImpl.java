package buscamines.impl;

import buscamines.Box;
import buscamines.BuscaminesApp;
import buscamines.BuscaminesContract;
import buscamines.BuscaminesContract.BuscaminesView;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;

/**
 *
 * @author usuari
 */


public class BuscaminesViewImpl implements Initializable, BuscaminesView {
    
    private int SIZE= 10;
    private GridPane grid;
    private BuscaminesModelImpl.Dificult DIFICULTAD = BuscaminesModelImpl.Dificult.EASY;
    private EventHandler<MouseEvent> mouseHandler;
    private EventHandler<ActionEvent> buttonHandler;
    private List<MyButton> buttons;
    private BuscaminesContract.BuscaminesPresenter presenter;
    private boolean sound = true;
    
    /* Sources*/
      private final Image BANDERA = new Image((getClass().getResourceAsStream("/buscamines/flag.png")), 14.0, 20.0, false, false);
//    private final Image MINA = new Image((getClass().getResourceAsStream("mine.png")), 14.0, 20.0, false, false);
//    private final AudioClip WIN = new AudioClip(getClass().getResource("win.wav").toString());
//    private final AudioClip EXPLOSION = new AudioClip(getClass().getResource("explosionMine.wav").toString());
//    private final AudioClip CLICK = new AudioClip(getClass().getResource("click.wav").toString());
    /*fin Sources*/
    
    
    @FXML
    private VBox vbox;
    
    @FXML
    private MenuItem exit, size1, size2, size3, dif1, dif2, dif3, onOff, helpCont, about;

    public BuscaminesViewImpl(Stage stage) throws IOException {
        initUI(stage);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    // UI
    private void initUI(Stage stage) {
        
        FXMLLoader loader = new FXMLLoader();
        loader.setController(this);
        loader.setLocation(getClass().getResource("Buscaminas.fxml"));
        
        grid = crearTablero();
        grid.setId("tablero");
        
        try{
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage.setTitle("Buscaminas Dani & David"); 
            
            /*Accion exit*/
            
            exit.setOnAction((ActionEvent t) -> {
                System.exit(0);
            });      
            
            
            /*Menu del tamaño del tablero*/
            
            
            size1.setOnAction((ActionEvent t) -> {
                SIZE=10;
               
                recargar(stage);
            });   
           
            size2.setOnAction((ActionEvent t) -> {
                SIZE=15;
                
                recargar(stage);
                System.out.println("Mediano");
            }); 
            
            size3.setOnAction((ActionEvent t) -> {
                SIZE=20;
                
                recargar(stage);
                System.out.println("Grande");
            });
            
            /*Menu de la dificultad*/
            
            dif1.setOnAction((ActionEvent t) -> {
                DIFICULTAD = BuscaminesModelImpl.Dificult.EASY;
                recargar(stage);
            });     
            
            dif2.setOnAction((ActionEvent t) -> {
                DIFICULTAD = BuscaminesModelImpl.Dificult.MEDIUM;
                recargar(stage);
            }); 
            
            dif3.setOnAction((ActionEvent t) -> {
                DIFICULTAD = BuscaminesModelImpl.Dificult.HARD;
                recargar(stage);
            });
            
            /*Menu help*/
            helpCont.setOnAction((ActionEvent t) -> {
                Alert helpAlert = new Alert(Alert.AlertType.INFORMATION, "Este es un mensaje de ayuda :D, Suerte!", ButtonType.CLOSE);
                helpAlert.setTitle("Help");
                helpAlert.showAndWait();
            });

            about.setOnAction((ActionEvent t) -> {
                Alert aboutAlert = new Alert(Alert.AlertType.INFORMATION, "Creado por Dani G. & David A. \n" + "INS La Ferreria \n", ButtonType.CLOSE);
                aboutAlert.setTitle("About");
                aboutAlert.setHeaderText("Buscaminas");
                aboutAlert.showAndWait();
            });
            /*Menu Sound*/
            
            onOff.setOnAction((ActionEvent t) -> {
            }); 
            
            /*Poner Banderita*/
            
            mouseHandler = (MouseEvent event) -> {
                MyButton myButton = (MyButton) event.getSource();
                MouseButton mouseButton = event.getButton();
                System.out.println("HolaCaracola");
                int pos = myButton.getPos();
                
                if (mouseButton == MouseButton.PRIMARY) {
                    if (!myButton.isFlag()) {
                        presenter.toUncover(pos);
                    }
                    
                } else if (!myButton.isFlag()) {
                    myButton.setFlag(true);
                    myButton.setGraphic(new ImageView(BANDERA));
                    showStage(stage, scene);
                    
                } else {
                    myButton.setFlag(false);
                    myButton.setGraphic(null);
                    showStage(stage, scene);
                }
            };

            vbox.getChildren().addAll(grid);
            setHandlerButtons();
            stage.setScene(scene);
            stage.show();
            
        }catch (IOException e) {
            throw new RuntimeException("loading", e);
        }
    }
    
     private void setHandlerButtons() {
        for (Node node : grid.getChildren()) {
            if (node instanceof MyButton) {
                //((Button) node).addEventHandler(ActionEvent.ACTION, buttonHandler);
                ((MyButton) node).setOnMouseClicked(mouseHandler);
            }
        }
    }

    public void showStage(Stage stage, Scene scene) {
        stage.setScene(scene);
        stage.setTitle("Buscamines");
        stage.show();
    }

    
    @Override
    public void setPresenter(BuscaminesContract.BuscaminesPresenter p) {
        this.presenter = p;
    }

    @Override
    public void UnCovered(Map<Integer, Integer> boxesDescovered) {
       
    }

    @Override
    public void overGame(List<Integer> posMines) {
        
        if (sound) {
            AudioClip explosion = new AudioClip(BuscaminesApp.class.getResource("explosionMine.wav").toString());
            explosion.play();
        }
        
       /*
        for (int y = 0; y < gridSize; y++) {
            for (int x = 0; x < gridSize; x++) {
                if (grid[x][y].hasBomb) {
                    grid[x][y].btn.setGraphic(new ImageView("mine.png"));
                    grid[x][y].btn.setDisable(true);
                }
            }
        }
        */

        Alert gameOver = new Alert(AlertType.INFORMATION);
        gameOver.setTitle("Game Over!");
        gameOver.setGraphic(new ImageView("mine.png"));
        gameOver.setHeaderText("Bomb Exploded!");
        gameOver.setContentText(
                "Oh no! You clicked on a bomb and caused all the bombs to explode! Better luck next time.");
        gameOver.showAndWait();
    }

    @Override
    public void win() {
        if (sound) {
            AudioClip winSound = new AudioClip(BuscaminesViewImpl.class.getResource("win.wav").toString());
            winSound.play();
        }
        Alert win = new Alert(AlertType.CONFIRMATION);
        win.setTitle("Win!");
        win.setGraphic(new ImageView("flag.png"));
        win.setHeaderText("Congratulations!");
        win.setContentText("You found all the bombs in " + 5 + " seconds.");
        win.showAndWait();
    }
    
    public GridPane crearTablero(){
        
        GridPane grid = new GridPane();
        int count = 0; 
            for(int i = 0; i < SIZE; i++){
                for(int j = 0; j < SIZE; j++){
                    MyButton button= new MyButton(count);
                    button.setPrefSize(30, 30);
                    grid.add(button, j, i);
                    count ++;
                }
            }
        return grid;
    }
    
    private void recargar(Stage stage) {

        vbox.getChildren().remove(1);
        vbox.getChildren().add(crearTablero());
        stage.sizeToScene();
    }

    public class MyButton extends Button {

        private boolean flag;
        private int pos;

        public int getPos() {
            return pos;
        }

        public MyButton(int pos) {
            flag = false;
            this.pos = pos;
        }

        public boolean isFlag() {
            return flag;
        }

        public void setFlag(boolean flag) {
            this.flag = flag;
        }

    }

}
