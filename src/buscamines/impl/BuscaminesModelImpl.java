package buscamines.impl;
import buscamines.Box;
import buscamines.BuscaminesContract;
import buscamines.BuscaminesContract.BuscaminesModelListener;
import buscamines.BuscaminesContract.BuscaminesPresenter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class BuscaminesModelImpl implements BuscaminesContract.BuscaminesModel {

    private Set<BuscaminesModelListener> listeners;
    private Box[][] grid;    
    public enum Dificult {
        EASY, MEDIUM, HARD;    
    }
    private GridPane gridpane;
    private List<Integer> posMines;
    private Map<Dificult, Integer> dificultPercents;
    private int size = 10;
    private Dificult dificult;
    private int pos; //position to discover
    private BuscaminesPresenter presenter;

    @Override
    public void start(int size, Dificult d) {
        grid = new Box[size][size];
                
        int count = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Box box = new Box(count);
                grid[i][j] = box;
                count++;
            }
        }
        
        
    }

    @Override
    public void play(int pos) {
        throw new RuntimeException("no implementat!");
    }

    @Override
    public void setPresenter(BuscaminesPresenter p) {
        this.presenter = p;
    }

    @Override
    public boolean addListener(BuscaminesModelListener listener) {
        return this.listeners.add(listener);
    }

    @Override
    public boolean removeListener(BuscaminesContract.BuscaminesModelListener listener) {
        return this.listeners.remove(listener);
    }

    @Override
    public Map<Integer, Integer> toUnCovered() {
//        Map<Integer, Integer> boxesToUnCover = new HashMap<>();
//        for (Box b : boxesDescovered) {
//            int position = b.getPos();
//            boxesToUnCover.put(b.getPos(), b.getMinesNeighbours());
//
//        }
//        return boxesToUnCover;
            throw new RuntimeException("loading");
    }

    @Override
    public int getRemaining() {
        int minas = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if(grid[i][j].getIsUnCovered()){
                    minas++;
                }
            }
        }
        return minas;
    }

    @Override
    public int getTotalMines() {
        int minas = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if(grid[i][j].getIsMine()){
                    minas++;
                }
            }
        }
        return minas;
    }

    @Override
    public boolean isOver() {
        
        
        
        return false;
    }

    @Override
    public Set<Integer> getSizes() {
         throw new RuntimeException("no implementat!");
    }
    
    private int getFile(){
        return pos/size;
    }
    private int getCol(){
        return pos%size;
    }


}
